import React, { useState } from 'react';
import { AppLoading } from 'expo';
import * as Font from 'expo-font';
import { Root } from "native-base";
import { AsyncStorage } from 'react-native';
import AuthNavigation from './navigation/AuthNavigation';
import AppNavigation from './navigation/AppNavigation';
import DicasScreen from './screens/app/DicasScreen';
import {Provider, inject, observer} from "mobx-react";
import pricebookStore from './store/store'

import { YellowBox } from 'react-native';
import _ from 'lodash';
YellowBox.ignoreWarnings(['Setting a timer']);
const _console = _.clone(console);
console.warn = message => {
  if (message.indexOf('Setting a timer') <= -1) {
    _console.warn(message);
  }
};

@observer
export default class App extends React.Component {
  state = {
    fontLoaded: false,
    jaIniciou: false
  };
  async componentDidMount(){
    await this.LoadFonts();
    await this.checkInit();
  }
  async checkInit() {
    //await AsyncStorage.removeItem('jaIniciou');
    const jaIniciou = await AsyncStorage.getItem('jaIniciou');
    this.setState({
      jaIniciou: jaIniciou
    })
  }
  async LoadFonts() {
    await Font.loadAsync({'Roboto_medium':require('./assets/fonts/Roboto-Medium.ttf')});
    this.setState({
      fontLoaded:true
    });   
  }
  
  render(){
    const { fontLoaded, jaIniciou } = this.state;
    const { user } = pricebookStore;
    if (!fontLoaded) return(<AppLoading/>);
    if (user===undefined) return(<AppLoading/>);
    if (!jaIniciou) return(
      <Root>
        <Provider store={pricebookStore}>
        <DicasScreen />
        </Provider>
      </Root>
    );
    if (pricebookStore.user===null) return(
      <Root>
        <Provider store={pricebookStore}>
        <AuthNavigation />
        </Provider>
      </Root>
    );
    return (
      <Root>
        <Provider store={pricebookStore}>
        <AppNavigation />
        </Provider>
      </Root>
    );
    }
}
