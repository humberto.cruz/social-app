import React, { Component } from 'react'
import LayoutApp from '../../components/LayoutApp';
import { View, Text, Thumbnail, List, ListItem, Left, Body } from 'native-base';
import _ from 'underscore';
import noImage from '../../assets/imgs/prod.jpg';

import { observer,inject } from "mobx-react"

type navigationType = {
    navigate: Function
}
type produtoType = {
    code: string,
    preco: number,
    identificacao: string
    data:number
}
type ProdutoScreenProps = {
    navigation: navigationType,
    store: any
}
type photoType = {
    url:string
    code:string
}
type ProdutoScreenStates = {
   
}

class BibliotecaScreen extends Component<ProdutoScreenProps, ProdutoScreenStates> {
    constructor(props){
        super(props);
    }
    async componentDidMount(){
        
    }
    _addZero(n:number){
        if (n<10) return '0'+n; else return n;
    }
    showDetail(produto:object){
        this.props.navigation.navigate('Detalhe',{code:produto.code,produto:produto});
    }
    render(){
        const {productsUniqueByUser} = this.props.store;
        
        if (productsUniqueByUser.length==0) return(
            <LayoutApp title={'Produto'} navigation={this.props.navigation}>
                <Text>Você ainda não registrou nenhum produto!</Text>
            </LayoutApp>
        );        
        return(
            <LayoutApp title={'Produto'} navigation={this.props.navigation}>
                    <List>
                    {productsUniqueByUser.map((p:produtoType,i:number)=>{
                        const photoURL = this.props.store.findProductPhotoUrl(p.code);
                        const data = new Date(p.data);
                        const fdata = this._addZero(data.getDate())+'/'+this._addZero(data.getMonth()+1)+'/'+data.getFullYear()+' '+this._addZero(data.getHours())+':'+this._addZero(data.getMinutes());
                        return(
                            <ListItem onPress={()=>this.showDetail(p)} thumbnail key={i}>
                                <Left>
                                {photoURL=='not-found'&&<Thumbnail style={{width:48, height:48, alignSelf:'center'}} source={noImage} />}
                                {photoURL!='not-found'&&<Thumbnail style={{width:48, height:48, alignSelf:'center'}} source={{uri:photoURL}} />}
                                </Left>
                                <Body>
                                    <Text>Código: {p.code}</Text>
                                    <Text>Último registro: {fdata}</Text>
                                    <Text style={{color:photoURL=='not-found'?'red':undefined}}>{photoURL!='not-found'?'Imagem meramente ilustrativa':p.identificacao}</Text>
                                </Body>
                            </ListItem>
                        )
                    })}
                    </List>
            </LayoutApp>
        );
    }
}

export default inject('store')(observer(BibliotecaScreen))