import React from "react";
import { Text, Thumbnail, Col, Row, Toast } from 'native-base';
import Button from '../../components/Button';
import LayoutApp from '../../components/LayoutApp';
import * as Permissions from 'expo-permissions';
import * as ImagePicker from 'expo-image-picker';
import UserImage from '../../assets/imgs/pricebook/avatar-orange.png'
import { AppLoading } from 'expo';

import { observer } from "mobx-react"
import pricebookStore from '../../store/store'

type MeusDadosScreenProps = {
  navigation: Object
}
type MeusDadosScreenStates = {
  currentUser:Object,
  photoURL: string,
  uploading: boolean,
  image: string,
  hasCameraRollPermission: boolean,
  hasCameraPermission: boolean
}

@observer
class MeusDadosScreen extends React.Component<MeusDadosScreenProps,MeusDadosScreenStates > {
    constructor(props){
      super(props);
      this.state = {
        currentUser:undefined,
        uploading:false,
        image:'',
        hasCameraPermission:false,
        hasCameraRollPermission:false
      }
    }
    componentDidMount () {
      this.getPermissionsAsyncCamera();
      this.getPermissionsAsyncCameraRoll();
      this.setState({
        currentUser: pricebookStore.user,
        photoURL: pricebookStore.user?pricebookStore.user.photoURL:null
      });
    }
    getPermissionsAsyncCamera = async () => {
      let { status } = await Permissions.askAsync(Permissions.CAMERA);
      this.setState({ hasCameraPermission: status === 'granted' });
    };
    getPermissionsAsyncCameraRoll = async () => {
      let { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      this.setState({ hasCameraRollPermission: status === 'granted' });
    };
    _takePhoto = async () => {
      const { hasCameraPermission } = this.state;
      if (!hasCameraPermission) return;
      let pickerResult = await ImagePicker.launchCameraAsync({
        allowsEditing: false,
        aspect: [4, 3],
      });
      this._handleImagePicked(pickerResult);
    }
    _pickImage = async () => {
      const { hasCameraRollPermission } = this.state;
      if (!hasCameraRollPermission) return;
      let pickerResult = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: false,
        mediaTypes:"Images",
        allowsMultipleSelection:false
      });
      this._handleImagePicked(pickerResult);
    }
    _handleImagePicked = async pickerResult => {
      try {
        this.setState({ uploading: true });
        if (!pickerResult.cancelled) {
          const uploadUrl = await this.uploadImageAsync(pickerResult.uri);
          pricebookStore.userProfileUpdate({photoURL:uploadUrl});
          this.setState({
            photoURL: uploadUrl
          });
        }
      } catch (e) {
        console.log(e);
        alert('Upload failed, sorry :(');
      } finally {
        this.setState({ uploading: false });
      }
    }
    async uploadImageAsync(uri:string) {
      // Why are we using XMLHttpRequest? See:
      // https://github.com/expo/expo/issues/2402#issuecomment-443726662
      const currentUser = pricebookStore.user;
      const blob = await new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.onload = function() {
          resolve(xhr.response);
        };
        xhr.onerror = function(e) {
          console.log(e);
          reject(new TypeError('Network request failed'));
        };
        xhr.responseType = 'blob';
        xhr.open('GET', uri, true);
        xhr.send(null);
      });
      const photoURL = await pricebookStore.uploadUserPhoto(currentUser.uid, blob);
      // We're done with the blob, close and release it
      blob.close();
      return photoURL;
    }
    async askEmailCheck(){
      pricebookStore.userCheckEmail().catch((err)=>{})
      .then(()=>{
        Toast.show({
          text:'Um email de verificação foi enviado, por favor verifique para efetivar seu acesso.'
        });
      })
    }  
    
    render() {          
      const { currentUser, photoURL } = this.state;
      if (!currentUser) return(<AppLoading/>);
      return (
        <LayoutApp title={'Home'} navigation={this.props.navigation}>
          {photoURL&&
          <Thumbnail style={{width:200,height:200,alignSelf:'center',margin:10}} source={{uri:photoURL}}/>
          }
          {!photoURL&&
          <Thumbnail style={{width:200,height:200,alignSelf:'center',margin:10}} source={UserImage}/>
          }
          <Row>
            <Col style={{padding:2}}>
              <Button onPress={this._takePhoto} block rounded><Text>Tirar Foto</Text></Button>
            </Col>
            <Col style={{padding:2}}>
              <Button onPress={this._pickImage} block rounded><Text>Enviar Arquivo</Text></Button>
            </Col>
          </Row>
          <Text>Email</Text>
          <Text>{currentUser.email} - {currentUser.emailVerified?'Verificado':'Pendente'}</Text>
          {!currentUser.emailVerified&&<Button onPress={this.askEmailCheck} block rounded><Text>Solicitar Email de Verificação</Text></Button>}
        </LayoutApp>
      );
    }
  }
  export default MeusDadosScreen;