import React from 'react';
import LayoutAuth from '../../components/LayoutAuth';
import { Text, Form, Item, Label, Input, Button, View, Card, CardItem, Toast, Thumbnail, Col } from 'native-base';
const LogoImg = require('../../assets/imgs/pricebook/PriceBookLogo.png');
import ui from '../../config/ui';
import { observer } from "mobx-react";
import pricebookStore from '../../store/store';

type RegisterScreenProps = {
  navigation: Object
}
type RegisterScreenStates = {
  email: string,
  password: string
}
@observer
class RegisterScreen extends React.Component<RegisterScreenProps,RegisterScreenStates> {
    static navigationOptions = {
        title: 'Cadatre-se por Email'
    };
    constructor(props){
      super(props);
      this.state = {
        email:'',
        password: ''
      };
      this.toLoginScreen = this.toLoginScreen.bind(this);
    }
    async componentDidMount(){
    }
    
    async doAuthCreate(){
      pricebookStore.userCreate(this.state.email,this.state.password)
      .then(()=>{}).catch((err)=>{
        Toast.show({
          text:'Erro na criação da conta, verifique se o email já está cadastrado.'
        });
      })
    }
    toLoginScreen(){
      this.props.navigation.navigate('Login');
    }
    render() {
      const { email, password } = this.state;         
      const { navigation } = this.props;
      return (
        <LayoutAuth navigation={navigation}>
          <Thumbnail style={{alignSelf:'center',width:150,height:200}} source={LogoImg} />
          <Card>
            <Form>
              <Item>
                  <Input placeholder={'Seu email'} keyboardType={'email-address'} autoCapitalize={'none'} value={email} onChangeText={(text)=>this.setState({email:text})} />
              </Item>
              <Item>
                  <Input placeholder={'Sua senha'} autoCapitalize={'none'} value={password} onChangeText={(text)=>this.setState({password:text})} />
              </Item>
            </Form>
            <View style={{height:10}} />
            <CardItem>
              <Col>
              <Button block rounded onPress={()=>this.doAuthCreate()} style={{backgroundColor:ui.headerStyle.backgroundColor}}><Text>Criar Conta</Text></Button>
              </Col>
            </CardItem>
          </Card>
          <View style={{height:10}} />
          <Button block rounded onPress={()=>this.toLoginScreen()} style={{backgroundColor:ui.headerStyle.backgroundColor}}><Text>Voltar</Text></Button>
        </LayoutAuth>
      );
    }
  }
  export default RegisterScreen;