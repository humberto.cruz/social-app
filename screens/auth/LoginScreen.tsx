import React from 'react';
import LayoutAuth from '../../components/LayoutAuth';
import { Text, Form, Item, Label, Input, Button, View, Card, CardItem, Thumbnail, Col, Icon, Row, Toast } from 'native-base';
const LogoImg = require('../../assets/imgs/pricebook/PriceBookLogo.png');
import ui from '../../config/ui';
import { observer } from "mobx-react";
import pricebookStore from '../../store/store';

type LoginScreenProps = {
  navigation: Object
};

type LoginScreenState = {
  email: string,
  passwd: string
};

@observer
class LoginScreen extends React.Component<LoginScreenProps,LoginScreenState> {
    static navigationOptions = {
        title: 'Pricebook - Login por Email'
    };
    constructor(props){
      super(props);
      this.state = {
        email:'',
        passwd: ''
      }
      this.toRegisterScreen = this.toRegisterScreen.bind(this);
      this.toPasswordScreen = this.toPasswordScreen.bind(this);
    }
    async doGoogleLogin(){
      pricebookStore.userLoginGoogle().catch((err)=>{})
      .then((user)=>{

      });
    }
    async doAuthLogin(){
      pricebookStore.userLogin(this.state.email, this.state.passwd)
      .then(()=>{})
      .catch((err)=>{
        Toast.show({
          text:'Não foi possível realizar o login, verifique seu email e senha.'
        });  
      });
    }
    toRegisterScreen(){
      this.props.navigation.navigate('Register');
    }
    toPasswordScreen(){
      const { email } = this.state;
      if (!email) {
        Toast.show({
          text:'Digite seu email.'
        });
        return false;
      }
      pricebookStore.userPassword(this.state.email).catch((err)=>{
      }).then(()=>{
        Toast.show({
          text:'Um Email para redifinição de senha foi enviado, verifique.'
        })
      })
    }
    render() {
      const { email, passwd } = this.state;
      const { navigation } = this.props;
      return (
        <LayoutAuth navigation={navigation}>
          <Thumbnail style={{alignSelf:'center',width:150,height:200}} source={LogoImg} />
          <Card>
            <Form>
              <Item>
                  <Input placeholder={'Seu email'} keyboardType={'email-address'} autoCapitalize={'none'} value={email} onChangeText={(text)=>this.setState({email:text})} />
              </Item>
              <Item>
                  <Input placeholder={'Sua senha'} autoCapitalize={'none'} value={passwd} onChangeText={(text)=>this.setState({passwd:text})} />
              </Item>
            </Form>
            <View style={{height:10}} />
            <CardItem>
              <Col>
                <Button iconLeft style={{backgroundColor:ui.headerStyle.backgroundColor}} rounded block onPress={()=>this.doAuthLogin()}>
                  <Text>Fazer Login</Text>
                </Button>
              </Col>
            </CardItem>
            <CardItem>
              <Col style={{marginRight:2}}>
                <Button style={{backgroundColor:ui.headerStyle.backgroundColor}} rounded block info onPress={()=>this.toPasswordScreen()}>
                  <Text>Senha ?</Text>
                </Button>
              </Col>
              <Col style={{marginLeft:2}}>
                <Button iconLeft style={{backgroundColor:ui.headerStyle.backgroundColor}} block rounded onPress={()=>this.toRegisterScreen()}>
                  <Icon name={'md-mail'} />
                  <Text>Criar Conta</Text>
                </Button>
              </Col>
            </CardItem>
          </Card>
          <View style={{height:10}} />
          <Row>
            <Col style={{padding:2}}>
              <Button iconLeft style={{backgroundColor:'#3C5A99'}} block rounded>
                <Icon name='logo-facebook' />
                <Text>Login Facebook</Text>
              </Button>
            </Col>
            <Col style={{padding:2}}>
              <Button iconLeft style={{backgroundColor:'#D84B37'}} block rounded>
                <Icon name={'logo-google'} />
                <Text>Login Google</Text>
              </Button>
            </Col>
          </Row>
        </LayoutAuth>
      );
    }
  }
  export default LoginScreen;