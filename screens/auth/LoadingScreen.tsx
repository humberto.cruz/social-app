import { Component } from 'react';
import { AppLoading } from 'expo';

class LoadingScreen extends Component {
    render(){
        return(<AppLoading/>);
    }
}

export default LoadingScreen;