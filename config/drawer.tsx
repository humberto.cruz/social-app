const user = [
    {
        key:'drawer-1',
        title:'Consultar Produto',
        icon: 'md-barcode',
        screen: 'Consultar',
        status: 'confirmado'
    },
    {
        key:'drawer-2',
        title:'Meus Dados',
        icon: 'md-person',
        screen: 'MeusDados',
        status: 'pendente'
    },
    {
        key:'drawer-3',
        title:'Dicas',
        icon: 'md-alert',
        screen: 'Dicas',
        status: 'pendente'
    },
    {
        key:'drawer-4',
        title:'Biblioteca de Produtos',
        icon: 'md-book',
        screen: 'Biblioteca',
        status: 'pendente'
    },
    {
        key:'drawer-5',
        title:'Termo de Serviço',
        icon: 'md-document',
        screen: 'TermoServico',
        status: 'pendente'
    }
];
const admin = [
    {
        title:'Dashboard',
        icon: 'md-scaner',
        screen: 'Dashboard'
    },
    {
        title:'Consultar Produto',
        icon: 'md-scaner',
        screen: 'Consultar'
    },
    {
        title:'Meus Dados',
        icon: 'md-person',
        screen: 'MeusDados'
    },
    {
        title:'Dicas',
        icon: 'md-scaner',
        screen: 'Dicas'
    }
];

export { user, admin};