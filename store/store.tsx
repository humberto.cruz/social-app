import { observable, action } from 'mobx';
import * as GoogleSignIn from 'expo-google-sign-in';

import _ from 'underscore';

import firebase from 'firebase';
import '@firebase/storage';
import '@firebase/firestore';
import '@firebase/auth';

// Carrega configuração do firebase
import firebaseConfig from '../config/firebase';
import { async } from '@firebase/util';
firebase.initializeApp(firebaseConfig); // Inicia app firebase
firebase.auth().onAuthStateChanged((user)=>{
	pricebookStore.user = user;
	pricebookStore.loadProductsByUser();
	pricebookStore.loadConfig();
})

class PricebookStore {
	// Variables
	@observable user = undefined;
	@observable productsUniqueByUser = [];
	@observable productsByUser = [];
	@observable productsPhotoURL = [];
	@observable loading = true;
	@observable loadProductsByUserSnap = undefined;
	@observable settings = {};
	
	// Actions
	@action userLogin (email:string,password:string) {
		return firebase.auth().signInWithEmailAndPassword(email, password);
	}
	@action async userLoginGoogle (){
		const credential = await GoogleSignIn.signInAsync().catch((err)=>console.log(err))
		.then((credential)=>{
			console.log(credential);
		});
		console.log(credential);
		return await firebase.auth().signInWithCredential(credential);
	}
	@action userLogout() {
		return firebase.auth().signOut();
	}
	@action userCreate (email:string,password:string) {
		return firebase.auth().createUserWithEmailAndPassword(email, password);
	}
	@action userPassword (email:string){
		return firebase.auth().sendPasswordResetEmail(email);
	}
	@action userProfileUpdate(profile:Object){
		return firebase.auth().currentUser.updateProfile(profile);
	}
	@action async uploadUserPhoto(uid,blob){
		const ref = firebase
        .storage()
        .refFromURL('gs://pricebook-2019.appspot.com/consumidores/'+uid+'.png');
	  const snapshot = await ref.put(blob);
	  return await ref.getDownloadURL();
	}
	@action userCheckEmail(){
		return firebase.auth().currentUser.sendEmailVerification();
	}

	// Carrega todos os produtos já "scaneados" pelo usuário
	@action loadProductsByUser(){
		const ref = firebase.firestore().collection('products');
		const query = ref.where('userId','==',this.user.uid);
		this.loadProductsByUserSnap = query.onSnapshot((res)=>{
			const docs = res.docs;
			let docsArray =  [];
			docs.map((d)=>{
				docsArray.push(d.data())
			});
			this.productsByUser = _.sortBy(docsArray,(o)=>o.data).reverse();
			this.loadUniqueProducts();
			this.loadAllProductsPhotos();
			
		},
		(err)=>{})
	}

	@action loadUniqueProducts(){
		let code = [];
		let products = [];
		this.productsByUser.map((p)=>{
			if(code.indexOf(p.code)==-1) {
				products.push(p);
				code.push(p.code);
			}
		});
		this.productsUniqueByUser = products;
	}

	@action loadProductsByCodeAndUser(code:string){
		let productsByCode = [];
		let codes = [];
		this.productsByUser.map((p)=>{
			if (p.code == code){
				productsByCode.push(p);
			}
		});
		return productsByCode;
	}

	@action async loadProductPhoto(code:string){
		const photoRef = firebase.storage().refFromURL('gs://pricebook-2019.appspot.com/produtos/'+code+'.png')
		return await photoRef.getDownloadURL()
	}

	@action async loadAllProductsPhotos(){
		const prods = this.productsUniqueByUser;
		this.productsPhotoURL = [];
		prods.map((p)=>{
			const photoRef = firebase.storage().refFromURL('gs://pricebook-2019.appspot.com/produtos/'+p.code+'.png')
			photoRef.getDownloadURL().then((url)=>{
				this.productsPhotoURL.push({
					url:url,
					code:p.code
				})
			}).catch((err)=>{
				this.productsPhotoURL.push({
					url:'not-found',
					code:p.code
				})
			});
		});
	}

	@action findProductPhotoUrl(code:string){
		const photoURL = _.findWhere(this.productsPhotoURL,{code:code});
		return photoURL.url;
	}

	@action saveProduct(preco,identificacao,promocao,data,location){
        const fs = firebase.firestore();
        const strpreco = preco.replace('.','').replace(',','');
        const dataConsulta = new Date();
        const products = {
          preco: (parseInt(strpreco)/100).toFixed(2),
          identificacao: identificacao,
          promocao: promocao,
          code: data.code,
          userId: firebase.auth().currentUser.uid,
		  data: dataConsulta.getTime(),
		  location: location
		}
		
		return fs.collection('products').add(products);
	}

	@action loadConfig(){
		const fs = firebase.firestore();
		const collection = fs.collection('settings');
		collection.onSnapshot((res)=>{
			let docs = {};
			res.docs.map((d)=>{
				docs[d.id] = d.data();
			});
			this.settings = docs;
		},
		(err)=>{})
	}
}
const pricebookStore = new PricebookStore();
export default pricebookStore;
