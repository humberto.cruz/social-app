import React from "react";
import { createDrawerNavigator, createAppContainer } from "react-navigation";

import Drawer from '../navigation/Drawer';

import ConsultarScreen from '../screens/app/ConsultarScreen';
import ScanScreen from "../screens/app/ScanScreen";
import ProductScreen from "../screens/app/ProdutoScreen";
import BibliotecaScreen from "../screens/app/BibliotecaScreen";
import DetalheScreen from "../screens/app/DetalheScreen";
import MeusDadosScreen from "../screens/app/MeusDadosScreen";
import DicasScreen from '../screens/app/DicasScreen';
import TermosScreen from '../screens/app/TermosScreen';
import defaultNavigationOptions from '../config/ui';

const AppNavigation = createDrawerNavigator(
    {
        Consultar: {
            screen: ConsultarScreen,
        },
        MeusDados: {
            screen: MeusDadosScreen
        },
        Scan: {
            screen: ScanScreen
        },
        Product: {
            screen: ProductScreen
        },
        Biblioteca: {
            screen: BibliotecaScreen
        },
        Detalhe: {
            screen: DetalheScreen
        },
        Dicas: {
            screen: DicasScreen
        },
        TermoServico: {
            screen: TermosScreen
        }
    },
    {
        initialRouteName:'Consultar',
        defaultNavigationOptions: defaultNavigationOptions,
        contentComponent: Drawer
    }
);
export default createAppContainer(AppNavigation);