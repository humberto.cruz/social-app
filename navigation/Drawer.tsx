import React from 'react';
import { DrawerItems, SafeAreaView, ScrollView } from 'react-navigation';
import { StyleSheet } from 'react-native';
import { Button, Text, Container, Toast, Header, Title, Content, List, ListItem, Body, Icon } from 'native-base';
import ui from '../config/ui';
import { user, admin } from '../config/drawer';

import { observer } from "mobx-react"
import pricebookStore from '../store/store'

class CustomDrawerContentComponent extends React.Component {
    constructor(props){
        super(props);
        this.goTo = this.goTo.bind(this);
        this.state = {
            currentUser: undefined
        }
    }
    componentDidMount(){
        
    }
    async goTo(screen){
        this.props.navigation.navigate(screen);
    }
    async doLogout(){
        pricebookStore.userLogout().then(function() {
          Toast.show({
            text:'Logout executado com sucesso!'
          });
        }).catch(function(error) {
          Toast.show({
            text:'Houve erro durante o logout!'
          });
        });
    }
    render(){
        return(
            <SafeAreaView style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
                <Container>
                    <Header noShadow noLeft style={{backgroundColor:ui.headerStyle.backgroundColor}}>
                        <Body>
                        <Title>Pricebook</Title>
                        </Body>
                    </Header>
                    <Content style={{padding:2}}>
                        <ScrollView>
                            <List>
                                {user.map((u)=>
                                    <ListItem key={u.key}>
                                        <Button dark iconLeft small block transparent onPress={()=>this.goTo(u.screen)}>
                                            <Icon name={u.icon}/>
                                            <Text>{u.title}</Text>
                                        </Button>
                                    </ListItem>
                                )}
                                <ListItem>
                                    <Button dark iconLeft small block transparent onPress={()=>this.doLogout()}>
                                        <Icon name={'md-exit'}/>
                                        <Text>Fazer Logout</Text>
                                    </Button>
                                </ListItem>
                            </List>
                        </ScrollView>
                    </Content>
                </Container>
                </SafeAreaView>
        );
    }
}
    

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: ui.headerStyle.backgroundColor
  },
});

export default CustomDrawerContentComponent;