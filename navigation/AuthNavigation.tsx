import React from "react";
import { createStackNavigator, createAppContainer } from "react-navigation";

import LoginScreen from '../screens/auth/LoginScreen';
import RegisterScreen from '../screens/auth/RegisterScreen';

import defaultNavigationOptions from '../config/ui';

const AuthNavigation = createStackNavigator(
    {
        Login: {
            screen: LoginScreen,
        },
        Register: {
            screen: RegisterScreen
        }
    },
    {
        defaultNavigationOptions: defaultNavigationOptions,
        headerMode: 'screen'
    }
);
export default createAppContainer(AuthNavigation);