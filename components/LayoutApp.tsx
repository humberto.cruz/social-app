import React from 'react';
import Constants from 'expo-constants';
import { Container, Content, Header, Body, Title, View, Left, Icon, Button, Right } from 'native-base';
import ui from '../config/ui';
import { KeyboardAvoidingView } from 'react-native';
type LayoutAppProps = {
    title: string,
    navigation: Object
}
type LayoutAppStates = {

}
class LayoutApp extends React.Component<LayoutAppProps,LayoutAppStates> {
    constructor(props){
        super(props);
    }
    render(){
        const headerBgColor = ui.headerStyle.backgroundColor;    
        return(
            <KeyboardAvoidingView behavior="padding" style={{ flex: 1 }}>
            <Container>
                <View style={{height:Constants.statusBarHeight, backgroundColor:headerBgColor}} />
                <Header noShadow style={{backgroundColor:headerBgColor}}>
                    <Left>
                        <Button onPress={()=>this.props.navigation.toggleDrawer()} transparent>
                            <Icon color={'#FFF'} name={'md-list'}/>
                        </Button>
                    </Left>
                    <Body>
                        <Title>{this.props.title}</Title>
                    </Body>
                    <Right/>
                </Header>
                <Content style={{padding:2}}>
                    {this.props.children}
                </Content>
            </Container>
            </KeyboardAvoidingView>
        );
    }
}

export default LayoutApp;