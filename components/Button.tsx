import React from 'react'
import { Button } from 'native-base'
export default class AppButton extends React.Component {
    render(){
        return(<Button rounded block {...this.props} />);
    }
}