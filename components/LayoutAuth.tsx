import React from 'react';
import { Container, Content, Text, StyleProvider } from 'native-base';
import { KeyboardAvoidingView, ScrollView } from 'react-native';

type LayoutAppProps = {
    navigation: Object
}

type LayoutAppStates = {
    
}

class LayoutApp extends React.Component<LayoutAppProps, LayoutAppStates> {
    render(){
        return(
            <KeyboardAvoidingView behavior="padding" style={{ flex: 1 }}>
                <ScrollView>
                    <Container>
                        <Content style={{padding:2}}>
                            {this.props.children}
                        </Content>
                    </Container>
                </ScrollView>
            </KeyboardAvoidingView>
        );
    }
}

export default LayoutApp;